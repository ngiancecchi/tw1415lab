<%-- 
    Document   : cookiejsp
    Created on : 27-nov-2009, 10.14.57
    Author     : bravetti
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@page session="false" %>

<%! int i =0; %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cookies in the Request</h1>
        <% Cookie[] cookies = request.getCookies();
        if (cookies==null) out.println("No cookies");
        else {
            for (int i=0; i<cookies.length; i++) {
            String name = cookies[i].getName();
            String value = cookies[i].getValue();
            out.println("cookie "+name+" = "+value+" <P>");
            }
        }%>

        <% if (i++ == 0) {
        Cookie c = new Cookie("login", "pippo");
        c.setMaxAge(180);
        response.addCookie(c); }
        %>        
    </body>
</html>
