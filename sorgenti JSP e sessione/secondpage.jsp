<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Second Page</h1>

        <h2>Menu</h2>
        <ul>
            <li>
        <A href="firstpage.jsp">firstpage.jsp</A>
            </li>
            <li>
        <A href="firstpage.jsp?set=yes">firstpage.jsp?set=yes</A>
            </li>
            <li>
        <A href="secondpage.jsp">secondpage.jsp</A>
            </li>
        </ul>

                        <h2>Request Info</h2>

        <ul>
            <li>
            Client HostName is
            <%= request.getRemoteHost() %>
            </li>
            <li>
            Client Port is
            <%= request.getRemotePort() %>
            </li>
        </ul>

        <h2>Session</h2>

        <ul>
            <li>
        Session ID is <%= session.getId() %>
            </li>
            <li>
        MaxInactiveInterval is <%= session.getMaxInactiveInterval() %>
            </li>

        </ul>

        <%! Object fieldAttribute; %>

        <h2>Attributes</h2>

        <ul>
            <li>
            Attribute "req_attribute" of "request" is
            "<%= request.getAttribute("req_attribute") %>"
            </li>
            <li>
            Field "fieldAttribute" of the JSP is
            "<%= fieldAttribute %>"
            </li>
            <li>
            Attribute "sess_attribute" of "session" is
            "<%= session.getAttribute("sess_attribute") %>"
            </li>
            <li>
            Attribute "app_attribute" of "application" is
            "<%= application.getAttribute("app_attribute") %>"
            </li>
        </ul>
    </body>
</html>
