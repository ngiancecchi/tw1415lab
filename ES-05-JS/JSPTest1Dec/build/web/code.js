// THIS JAVASCRIPT WORKS!

window.onload = function() {
    document.getElementById("submit").onclick = function(){
        return control(this.form);
    };
};

function showErrors(check,spanId) {
    var span = document.getElementById(spanId);
    span.style.visibility = !check;
}

function validateNames(str){
    var regexp = /^[a-zA-Z]+$/;
    return regexp.test(str);
}

function validatePhone(str){
    var regexp = /^[\+]\d{6,}$/; //\d è come fare [0-9]
    return regexp.test(str);
}

function control(form){
    
    if(!validateNames(form.nome.value)){
        alert("Nome non valido");
        showErrors(false,"err1");
        return false;
    }
    
    if(!validateNames(form.cognome.value)){
        alert("Cognome non valido");
        showErrors(false,"err2");
        return false;
    }
    
    if(!validatePhone(form.tel.value)){
        alert("Numero di telefono non valido");
        showErrors(false,"err3");
        return false;
    }
    
    return true;
}