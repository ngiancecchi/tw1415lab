<%-- 
    Document   : index
    Created on : Feb 6, 2015, 11:56:44 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ferrovie Universitarie</title>
        <link rel='stylesheet' href="style.css" type='text/css' />
    </head>
    <body>
        
        <div id='container'>
            
            <div id='top'>
                <h1>Ferrovie Universitarie</h1>
                <img src='treno.jpg' alt='Ferrovie Universitarie' />
            </div>
            
            <div id='navbar'>
                <a href='index.jsp'>Home</a>
                <a href='login.jsp'>Login</a>
                <a href='registration.jsp'>Registrazione</a>
            </div>
            
            <div id='content'>
                <p>
                    Il treno &egrave;, nell'accezione pi&ugrave; comune del 
                    termine, un mezzo di trasporto pubblico atto alla 
                    circolazione sulle ferrovie composto da un insieme di 
                    elementi identificabili, uniti permanentemente o 
                    temporaneamente a formare un unico convoglio. La parola 
                    treno deriva dal latino Trahere: tirare; il termine si 
                    &egrave; modificato nel tempo tramite il francese provenzale 
                    (train). Il treno &egrave; stato il primo vero e proprio 
                    veicolo di trasporto di massa, e in molti casi ha 
                    rappresentato un punto di svolta per l'evoluzione 
                    industriale delle nazioni ottocentesche, arrivando quindi a 
                    rivestire per molti anni un ruolo centrale nella struttura 
                    politica, economica e sociale delle nazioni, nonch&egrave;
                    conquistando un posto di primo piano nell'immaginario 
                    collettivo.
                </p>
            </div>
        </div>
        
        <div id='banner'>
            Benvenuto Coglione!
        </div>
        
        
    </body>
</html>
