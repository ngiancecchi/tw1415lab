<%-- 
    Document   : checkreg
    Created on : Jan 26, 2015, 10:02:00 PM
    Author     : nicolagiancecchi
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
       <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            
            <header>
                <%@ include file="header.jspf" %>
            </header>
            
            <nav>
                <%@ include file="nav.jspf" %>
            </nav>
            
            
            <article>
                <%
                    String nome = (String)request.getParameter("nome");
                    String cognome = (String)request.getParameter("cognome");
                    String email = (String)request.getParameter("email");
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    
                    Map<String,String[]> registrazioni = (Map<String,String[]>)application.getAttribute("registrazioni");
                    
                    if(registrazioni==null){
                        registrazioni = new HashMap<String,String[]>();
                        application.setAttribute("registrazioni", registrazioni);
                    }
                    
                    registrazioni.put(username, new String[]{password,email,nome,cognome});
                    session.invalidate();
                    %>
                    
                    <h1>
                        Registrazione avvenuta con successo
                    </h1>
                    
                    <p>L'utente <%= nome%> <%= cognome%> &egrave; iscritto correttamente al sistema.</p>
            </article>
            
            <footer>
                
            </footer>
        </div>
    </body>
</html>
