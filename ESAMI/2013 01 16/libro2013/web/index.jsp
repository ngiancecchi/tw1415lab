<%-- 
    Document   : index
    Created on : Jan 26, 2015, 10:01:21 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            
            <header>
                <%@ include file="header.jspf" %>
            </header>
            
            <nav>
                <%@ include file="nav.jspf" %>
            </nav>
            
            
            <article>
                
                <div id="tableContainer">
                    <div id="tableHeader">
                        <div class="cell">Lun</div>
                        <div class="cell">Mar</div>
                        <div class="cell">Mer</div>
                        <div class="cell">Gio</div>
                        <div class="cell">Ven</div>
                        <div class="cell">Sab</div>
                        <div class="cell">Dom</div>
                    </div>
                    
                    <div class="tableRow">
                        <div class="cell"></div>
                        <div class="cell">1</div>
                        <div class="cell">2</div>
                        <div class="cell">3</div>
                        <div class="cell">4</div>
                        <div class="cell">5</div>
                        <div class="sunday">6</div>
                    </div>
                    
                    <div class="tableRow">
                        <div class="cell">7</div>
                        <div class="cell">8</div>
                        <div class="cell">9</div>
                        <div class="cell">10</div>
                        <div class="cell">11</div>
                        <div class="cell">12</div>
                        <div class="sunday">13</div>
                    </div>
                    
                    <div class="tableRow">
                        <div class="cell">14</div>
                        <div class="cell">15</div>
                        <div id="today">16</div>
                        <div class="cell">17</div>
                        <div class="cell">18</div>
                        <div class="cell">19</div>
                        <div class="sunday">20</div>
                    </div>
                    
                    <div class="tableRow">
                        <div class="cell">21</div>
                        <div class="cell">22</div>
                        <div class="cell">23</div>
                        <div class="cell">24</div>
                        <div class="cell">25</div>
                        <div class="cell">26</div>
                        <div class="sunday">27</div>
                    </div>
                    
                    <div class="tableRow">
                        <div class="cell">28</div>
                        <div class="cell">29</div>
                        <div class="cell">30</div>
                        <div class="cell">31</div>
                        <div class="cell"></div>
                        <div class="cell"></div>
                        <div class="sunday"></div>
                    </div>
                    
                </div>
                
                <aside>
                    <p><span class="underlined">John Howard Carpenter (Carthage, <span class="bolditalic">16 gennaio 1948</span>)</span> &egrave;
                    un regista, sceneggiatore, compositore, attore, produttore 
                    cinematografico e montatore statunitense.</p>
                    <p>Tra i suoi lavori pi&ugrave; famosi si annoverano Distretto 13: 
                        le brigate della morte (1976), Halloween, la notte delle 
                        streghe (1978), 1997: fuga da New York (1981), La cosa 
                        (1982), Christine, la macchina infernale (1983), Essi 
                        vivono (1988) e Il seme della follia (1994).</p>
                    <p>Carpenter afferma spesso di essere stato influenzato dalle 
                        opere di Howard Hawks, Alfred Hitchcock e dalla serie 
                        televisiva Ai confini della realt&agrave;.</p>
                    <p>I suoi film glorificano spesso degli anti-eroi, personaggi 
                        di estrazione proletaria e i suoi soggetti hanno spesso 
                        tematiche che riflettono una forte critica sulla societ&agrave; 
                        capitalistica americana.</p>
                </aside>
            </article>
                
            
            
            <footer>
                
                
            </footer>
          
        </div>
    </body>
</html>
