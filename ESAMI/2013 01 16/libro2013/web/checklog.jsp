<%-- 
    Document   : checklog
    Created on : Jan 26, 2015, 10:02:09 PM
    Author     : nicolagiancecchi
--%>

<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
       <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            
            <header>
                <%@ include file="header.jspf" %>
            </header>
            
            <nav>
                <%@ include file="nav.jspf" %>
            </nav>
            
            
            <article>
                <%
                    Map<String,String[]> registrazioni = (Map<String,String[]>)application.getAttribute("registrazioni");
                    
                    if(registrazioni!=null){
                        String[] values = registrazioni.get(request.getParameter("username"));
                        if(values != null && values[0].equals(request.getParameter("password"))){
                          //UN e PW ok  
                    %>
                    
                    <h1>
                        Login avvenuto con successo!
                    </h1>
                    
                    <p>Bentornato/a, <%= values[2]%> <%= values[3]%>!</p>
                    
                    <%
                        session.setAttribute("nome", values[2]);
                        session.setAttribute("cognome", values[3]);
                        
                        } else {
                    %>
                    
                    <h1>
                        Credenziali errate.
                    </h1>
                    
                    <p>Credenziali di accesso errate. Vuoi 
                        <a href="login.jsp">riprovare</a> o 
                        <a href="index.jsp">tornare alla homepage?</a></p>
                    
                    <%
                        }
                        
                    }
                    %>
            </article>
            
            <footer>
                
            </footer>
        </div>
    </body>
</html>
