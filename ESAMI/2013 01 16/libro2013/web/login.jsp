<%-- 
    Document   : login
    Created on : Jan 26, 2015, 10:01:41 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
       <link rel="stylesheet" href="style.css" type="text/css" />
        <script type="text/javascript" src="fieldcheck.js" ></script>
    </head>
    <body>
        <div id="container">
            
            <header>
                <%@ include file="header.jspf" %>
            </header>
            
            <nav>
                <%@ include file="nav.jspf" %>
            </nav>
            
            
            <article>
                <form action="checklog.jsp" method="get">
                    <div>
                    <label for="username">Username:</label>
                    <input type="text" name="username" />
                    </div> 
                    
                    <div>
                    <label for="password">Password:</label>
                    <input type="password" name="password" />
                    </div>
                    
                    <div>
                    <input type="submit" id="submit" value="Salva" />
                    </div>
                </form>
            </article>
            
            <footer>
                
            </footer>
        </div>
    </body>
</html>
