<%-- 
    Document   : index
    Created on : Feb 4, 2015, 3:30:00 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script type='text/javascript' src='formcheck.js' ></script>
    </head>
    <body>
        
        <header>
            <%@include file="header.jspf" %>
        </header>
        
        <div id="container">
            
            <form action="checklog.jsp" method="post">
                <div class="entry">
                    Username:
                    <input type="text" name="username" placeholder="username" />
                </div>
                
                <div class="entry">
                    Password:
                    <input type="password" name="password" placeholder="password" />
                </div>
                
                <input type="submit" id="submit" value="Login" />
                
                
            </form>
            
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
            
        </div>
        
        
    </body>
</html>
