<%-- 
    Document   : index
    Created on : Feb 4, 2015, 3:30:00 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <%
                  Boolean userLogged = false;
                  String rqUsername = (String)request.getParameter("username");
                  String rqPassword = (String)request.getParameter("password");

                  
                  if(rqUsername.equals("lettore5") && rqPassword.equals("a4a4")){
                      userLogged=true;
                      session.setAttribute("username", rqUsername);
                  }
                %>  
        <header>
            <%@include file="header.jspf" %>
        </header>
        
        <div id="container">
            
            <div id="advice">
                
              <% if(userLogged){ %>
                        <h1>Login eseguito con successo!</h1>
                        L'utente <%= rqUsername %> ha effettuato la login.
                        <%
                  } else {
                        %>
                        <h1>Credenziali errate</h1>
                        Errore di login.
                   <%
                  }
             %>     
            
            
            
            </div>
            
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
            
        </div>
        
        
    </body>
</html>
