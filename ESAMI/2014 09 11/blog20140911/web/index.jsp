<%-- 
    Document   : index
    Created on : Feb 4, 2015, 3:30:00 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <%
            String username = (String)session.getAttribute("username");
            Boolean userLogged = (username!=null);
            %>
        <header>
            <%@include file="header.jspf" %>
        </header>
        
        <div id="container">
            
            <aside>
                <h1>Titolo</h1>
                <p>Ut auctor, ante quis fringilla interdum, arcu sapien 
                    vestibulum diam, quis auctor lorem felis vel sapien. 
                    Vivamus ac felis libero. Nam elementum, sem non ornare 
                    hendrerit, elit ligula elementum dolor, et 
                    ullamcorper purus tellus nec neque. Nam feugiat non leo in 
                    eleifend. Nunc porta sit amet nisl in sollicitudin. 
                    Sed interdum, sem nec laoreet porttitor, risus ipsum 
                    eleifend metus, id sollicitudin risus  metus quis magna. 
                    Etiam sem lacus, viverra quis ipsum quis, mattis scelerisque 
                    ligula. Maecenas ornare rutrum diam sit amet laoreet. Cras 
                    eget lectus quis ante vehicula rutrum. In auctor turpis nec 
                    pharetra scelerisque. Cras accumsan tellus vel sapien dictum, 
                    eu hendrerit turpis molestie.</p>
            </aside>
            
            <section>
                <article>
                    <h1>Titolo</h1>
                    <img src="news.jpg" alt="Immagine"/>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Etiam in adipiscing magna. 
                        Ut vitae euismod sapien, ac accumsan lectus. Aliquam vel 
                        elit molestie, congue odio sed, gravida elit. 
                        Ut consequat tortor eu tortor tristique, bibendum 
                        suscipit est aliquet. Mauris iaculis nulla arcu, 
                        id auctor nisl facilisis eu. </p>
                    
                    <p class="author"><span class="bold">Autore</span>: Mario Rossi</p>
                    
                    <%if(userLogged){%>
                    <a class="like" href="mipiace.jsp?autore=rossi">
                        <%
                        String like = (String)application.getAttribute("rossi");
                            if(like != null && like.equals(username)) {%> 
                            Non mi piace più
                        <% } else { %> Mi piace <% } %>
                    </a>
                    <%}%>
                </article>
                
                <article>
                    <h1>Titolo</h1>
                    <img src="news.jpg" alt="Immagine"/>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Etiam in adipiscing magna. 
                        Ut vitae euismod sapien, ac accumsan lectus. Aliquam vel 
                        elit molestie, congue odio sed, gravida elit. 
                        Ut consequat tortor eu tortor tristique, bibendum 
                        suscipit est aliquet. Mauris iaculis nulla arcu, 
                        id auctor nisl facilisis eu. </p>
                    
                    <p class="author"><span class="bold">Autore</span>: Luca Bianchi</p>
                    
                    <%if(userLogged){%>
                    <a class="like" href="mipiace.jsp?autore=bianchi">
                        <%
                        String like = (String)application.getAttribute("bianchi");
                            if(like != null && like.equals(username)) {%> 
                            Non mi piace più
                        <% } else { %> Mi piace <% } %>
                    </a>
                    <%}%>
                </article>
            </section>
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
            
        </div>
        
        
    </body>
</html>
