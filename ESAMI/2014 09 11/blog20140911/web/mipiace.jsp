<%-- 
    Document   : index
    Created on : Feb 4, 2015, 3:30:00 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <header>
            <%@include file="header.jspf" %>
        </header>
        
        <div id="container">
            
            <div id='advice'>
            <%
                String autore = (String)request.getParameter("autore");
                String uname = (String)session.getAttribute("username");
                String autoreLiked = (String)application.getAttribute(autore);
                
                if(uname!=null && autore!=null){
                    if(autoreLiked!=null && autoreLiked.equals(uname)){
                        application.setAttribute(autore, "");
                        %>
                        <h1>Not liked anymore :(</h1>
                        <span class="bold">Non mi piace più</span> indicato per l'autore <%= autore %>.
                        <%
                    } else {
                        application.setAttribute(autore, uname);
                        %>
                        <h1>Liked! :)</h1>
                        <span class="bold">Mi piace</span> indicato per l'autore <%= autore %>.
                        <%
                    }
                }
                %>
            
            </div>
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
            
        </div>
        
        
    </body>
</html>
