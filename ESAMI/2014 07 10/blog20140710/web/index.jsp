<%-- 
    Document   : index
    Created on : Feb 5, 2015, 10:08:02 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog (esame 10/07/14)</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <%
            String userlogged = (String)session.getAttribute("username");
        %>
        
        
        <header>
            <%@include file="header.jspf" %>
        </header>
        
        <div id="container">
            
            <section>
                <article>
                    <h1>Titolo</h1>
                    <div class="voto">
                        <%
                            Integer votiRossi = (Integer)application.getAttribute("rossi");
                            if(votiRossi==null){
                                votiRossi=1;
                                application.setAttribute("rossi", votiRossi);
                            }
                        %>
                        <p>
                            <span class="bold">Voto</span>: <%= votiRossi %>
                        </p>
                        <% 
                            if(userlogged != null){
                                %>
                        <a href="voto.jsp?autore=rossi" />Vota</a>
                    <% } %>
                    </div>
                    
                    <img src="news.jpg" alt="News" />
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Etiam in adipiscing magna.  Ut vitae euismod sapien, ac 
                        accumsan lectus. Aliquam vel elit molestie, congue odio 
                        sed, gravida elit. Ut consequat tortor eu tortor 
                        tristique, bibendum suscipit est aliquet. Mauris iaculis 
                        nulla arcu, id auctor nisl facilisis eu. 
                    </p>
                    <p class="author"><span class="bold">Autore</span>: Mario Rossi</p>
                </article>
                <article>
                    <h1>Titolo</h1>
                    
                    <div class="voto">
                        <%
                            Integer votiBianchi = (Integer)application.getAttribute("bianchi");
                            if(votiBianchi==null){
                                votiBianchi=1;
                                application.setAttribute("bianchi", votiBianchi);
                            }
                        %>
                        
                        <p>
                            <span class="bold">Voto</span>: <%= votiBianchi %>
                        </p>
                        <% 
                            if(userlogged != null){
                                %>
                        <a href="voto.jsp?autore=bianchi" />Vota</a>
                    <% } %>
                    </div>
                    
                    <img src="news.jpg" alt="News" />
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Etiam in adipiscing magna.  Ut vitae euismod sapien, ac 
                        accumsan lectus. Aliquam vel elit molestie, congue odio 
                        sed, gravida elit. Ut consequat tortor eu tortor 
                        tristique, bibendum suscipit est aliquet. Mauris iaculis 
                        nulla arcu, id auctor nisl facilisis eu. 
                    </p>
                    <p class="author"><span class="bold">Autore</span>: Luca Bianchi</p>
                </article>
            </section>
            <aside>
                <h1>Titolo</h1>
                <p>
                    Ut auctor, ante quis fringilla interdum, arcu sapien 
                    vestibulum diam, quis auctor lorem felis vel sapien. 
                    Vivamus ac felis libero. Nam elementum, sem non ornare 
                    hendrerit, elit ligula elementum dolor, et ullamcorper purus 
                    tellus nec neque.  Nam feugiat non leo in eleifend. Nunc 
                    porta sit amet nisl in sollicitudin. Sed interdum, sem nec 
                    laoreet porttitor, risus ipsum eleifend metus, id 
                    sollicitudin risus metus quis magna. Etiam sem lacus, 
                    viverra quis ipsum quis, mattis scelerisque ligula. Maecenas 
                    ornare rutrum diam sit amet laoreet. Cras eget lectus quis 
                    ante vehicula rutrum. In auctor turpis nec pharetra 
                    scelerisque. Cras accumsan tellus vel sapien dictum, eu 
                    hendrerit turpis molestie.
                </p>
                
            </aside>
            
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        
        </div>
        
    </body>
</html>
