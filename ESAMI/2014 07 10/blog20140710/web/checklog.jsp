<%-- 
    Document   : index
    Created on : Feb 5, 2015, 10:08:02 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog (esame 10/07/14)</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <%
            Boolean logged = false;
            
            String uname = (String)request.getParameter("username");
            String psw = (String)request.getParameter("password");
            
            if(psw.equals("12345")){
                logged = true;
                session.setAttribute("username", uname);
            }
            %>
        
        <header>
            <%@include file="header.jspf" %>
        </header>
        
        <div id="container">
        
            <div id='loginmessage'>
            <%
                if(logged){
            %>
            <h1>Login effettuato con successo</h1>
            <p>L'utente <%= uname%> ha effettuato il login.</p>
            <%
                } else {
            %>
            <h1>Credenziali errate</h1>
            <p>Errore di login.</p>
            <%
                }
            %>
            
            </div>
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        
        </div>
        
    </body>
</html>
