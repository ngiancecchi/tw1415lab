/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


window.onload = function(){
    document.getElementById("submit").onclick = function(){
        return check(this.form);
    };
};

function checkVoid(string){
    var regexp = /^./;
    return regexp.test(string);
}

function checkUsername(string){
    var regexp = /^([a-z]{1,})([.]{1})([a-z]{1,})([0-9]{1,})$/;
    return regexp.test(string);
}


function check(form){
    
    var valid = true;
    
    if(!checkVoid(form.username.value) || !checkUsername(form.username.value)){
        valid = false;
    }
    
    if(!checkVoid(form.password.value)){
        valid = false;
    }
    
    if(valid===false){
        alert("Ci sono controlli errati");
        return false;
    }
    
    return true;
}