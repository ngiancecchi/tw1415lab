<%-- 
    Document   : index
    Created on : Feb 5, 2015, 10:08:02 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog (esame 10/07/14)</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script type='text/javascript' src='checkform.js'></script>
    </head>
    <body>
        
        <header>
            <%@include file="header.jspf" %>
        </header>
        
        <div id="container">
            
            <form action="checklog.jsp" method="post">
                
                <div class="entry">
                    Username:
                    <input type="text" name="username" placeholder="username" />
                </div>
                
                <div class="entry">
                    Password:
                    <input type="password" name="password" placeholder="password" />
                </div>
               
                <input type="submit" id="submit" name="submit" value="Login" />
                
            </form>
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        
        </div>
        
    </body>
</html>
