<%-- 
    Document   : index
    Created on : Feb 5, 2015, 10:08:02 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog (esame 10/07/14)</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <%
            String author = (String)request.getParameter("autore");
            if(author!=null){
                Integer voti = (Integer)application.getAttribute(author);
                application.setAttribute(author, voti+1);
            }
            %>
        
        <header>
            <%@include file="header.jspf" %>
        </header>
        
        <div id="container">
        
            <div id='loginmessage'>
                <h1>+1!</h1>
                <p>Voto conteggiato per l'autore <%= author%></p>
            
            </div>
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        
        </div>
        
    </body>
</html>
