<%-- 
    Document   : checklog
    Created on : Feb 1, 2015, 2:56:26 PM
    Author     : nicolagiancecchi
--%>

<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog &raquo; Login</title>
        
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
         <%@ include file="header.jspf" %>
         
         <div id="loginContainer">
             <article>
                 
             
                 <%
                     String username = (String)request.getParameter("username");
                     String password = (String)request.getParameter("password");
                     
                     Map<String,String> users = (Map<String,String>)application.getAttribute("users");
                     
                     if(users==null){
                         users = new HashMap<String,String>();
                         users.put("rossim", "rossino");
                         users.put("bianchil", "bianchino");
                     }
                     
                     String userPassword = users.get(username);
                     if(userPassword != null && userPassword.equals(password)){
                         
                     %>
                     Login effettuato con successo.
                     
             
                     <%
                         session.setAttribute("userLogged", username);
                     } else {
                         %>
                         Credenziali errate.
                         <%
                     }
                         %>
             </article>
             
         </div>
         
         <%@ include file="footer.jspf" %>
    </body>
</html>