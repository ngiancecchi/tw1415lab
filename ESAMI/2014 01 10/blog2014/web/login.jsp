<%-- 
    Document   : login
    Created on : Feb 1, 2015, 1:39:42 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog &raquo; Login</title>
        
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="formcheck.js" type="text/javascript"></script>
    </head>
    <body>
        
         <%@ include file="header.jspf" %>
         
         <div id="loginContainer">
             <article>
             <h1>Login</h1>
             
             <form action="checklog.jsp" method="post">
                 <p>
                 <label for="username">Username</label>
                 <input type="text" name="username" />
                 </p>
                 
                 <p>
                 <label for="password">Password</label>
                 <input type="password" name="password" />
                 </p>
                
                 <p>
                 <input type="submit" name="submit" id="submit" value="Esegui il login" />
                 </p>
                 
             </form>
             </article>
             
         </div>
         
         <%@ include file="footer.jspf" %>
    </body>
</html>
