/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


window.onload = function(){
    document.getElementById("submit").onclick = function(){
        return check(this.form);
    };
};

function checkStringEmpty(string){
    //return string.length > 0;
    var regex = /^./;
    return regex.test(string);
}

function checkUsername(string){
    var regex = /^([a-zA-Z]{1})(\w+)$/;
    return regex.test(string);
    
    /*lo  username sia composto da un 
     * carattere alfabetico (maiuscolo o minuscolo) 
 * seguito da soli caratteri alfanumerici. */
}

function check(form){
    
    var usernameResult = checkStringEmpty(form.username.value) && checkUsername(form.username.value);
    var passwordResult = checkStringEmpty(form.password.value);
    
    if(!usernameResult || !passwordResult){
        alert("Ci sono controlli errati");
        return false;
    }
    
    return true;
    
}
