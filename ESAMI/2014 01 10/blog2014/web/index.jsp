<%-- 
    Document   : home
    Created on : Feb 1, 2015, 1:39:31 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MultiAuthorBlog &raquo; Home Page</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
         <%@ include file="header.jspf" %>
         
         <div id="container">
        
          <aside>
            <h1>Titolo</h1>
            <p>Ut auctor, ante quis fringilla interdum, arcu sapien vestibulum diam, quis auctor lorem felis vel sapien. 
	Vivamus ac felis libero. Nam elementum, sem non ornare hendrerit, elit ligula elementum dolor, et ullamcorper purus tellus nec neque. 
	Nam feugiat non leo in eleifend. Nunc porta sit amet nisl in sollicitudin. 
	Sed interdum, sem nec laoreet porttitor, risus ipsum eleifend metus, id sollicitudin risus metus quis magna. 
	Etiam sem lacus, viverra quis ipsum quis, mattis scelerisque ligula. Maecenas ornare rutrum diam sit amet laoreet. 
	Cras eget lectus quis ante vehicula rutrum. In auctor turpis nec pharetra scelerisque. Cras accumsan tellus vel sapien dictum, 
        eu hendrerit turpis molestie.</p>
            
            <h1>Titolo</h1>
            <p>Ut auctor, ante quis fringilla interdum, arcu sapien vestibulum diam, quis auctor lorem felis vel sapien. 
	Vivamus ac felis libero. Nam elementum, sem non ornare hendrerit, elit ligula elementum dolor, et ullamcorper purus tellus nec neque. 
	Nam feugiat non leo in eleifend. Nunc porta sit amet nisl in sollicitudin. 
	Sed interdum, sem nec laoreet porttitor, risus ipsum eleifend metus, id sollicitudin risus metus quis magna. 
	Etiam sem lacus, viverra quis ipsum quis, mattis scelerisque ligula. Maecenas ornare rutrum diam sit amet laoreet. 
	Cras eget lectus quis ante vehicula rutrum. In auctor turpis nec pharetra scelerisque. Cras accumsan tellus vel sapien dictum, 
        eu hendrerit turpis molestie.</p>
            
        </aside>
             
        <section>
            <article>
                <h1>Titolo</h1>
                <img src="news.jpg" alt="News" />
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in adipiscing magna. 
	Ut vitae euismod sapien, ac accumsan lectus. Aliquam vel elit molestie, congue odio sed, gravida elit. 
	Ut consequat tortor eu tortor tristique, bibendum suscipit est aliquet. Mauris iaculis nulla arcu, 
	id auctor nisl facilisis eu. Suspendisse sodales volutpat pharetra. Donec sollicitudin consectetur felis quis pharetra. 
	Maecenas rhoncus ipsum vitae rhoncus tempus. Curabitur consectetur sollicitudin ante, sed ultrices ante vulputate eu. 
	Maecenas dictum malesuada mauris a pellentesque.</p>
        
                <p><span>Autore:</span> Mario Rossi
                <%
                    String attribute = (String)session.getAttribute("userLogged");
                    if(attribute != null && attribute.equals("rossim")){
                        %>
                <input type="button" value="MODIFICA" />
                <%
                    }
                    %>
                </p> 
            </article>
            <article>
                <h1>Titolo</h1>
                <img src="news.jpg" alt="News" />
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in adipiscing magna. 
	Ut vitae euismod sapien, ac accumsan lectus. Aliquam vel elit molestie, congue odio sed, gravida elit. 
	Ut consequat tortor eu tortor tristique, bibendum suscipit est aliquet. Mauris iaculis nulla arcu, 
	id auctor nisl facilisis eu. Suspendisse sodales volutpat pharetra. Donec sollicitudin consectetur felis quis pharetra. 
	Maecenas rhoncus ipsum vitae rhoncus tempus. Curabitur consectetur sollicitudin ante, sed ultrices ante vulputate eu. 
        Maecenas dictum malesuada mauris a pellentesque.</p>
                
                <p><span>Autore:</span> Luca Bianchi
                <%
                    if(attribute != null && attribute.equals("bianchil")){
                        %>
                <input type="button" value="MODIFICA" />
                <%
                    }
                    %>
                </p>
            </article>
        </section>
             
             
                        
             
         </div>
         
         <%@ include file="footer.jspf" %>
         
    </body>
</html>
