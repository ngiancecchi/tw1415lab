<%-- 
    Document   : index
    Created on : Feb 6, 2015, 9:05:47 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Maya</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <%
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                        String maya = (String)application.getAttribute("nomemaya");
                    
                    Boolean valid = false;
                    
                    if(username.equals(application.getAttribute("username")) && 
                            password.equals(application.getAttribute("password"))){
                        valid = true;
                        
                        session.setAttribute("username", username);
                        session.setAttribute("nomemaya", maya);
                        session.setAttribute("uinal", application.getAttribute("uinal"));
                        session.setAttribute("tun", application.getAttribute("tun"));
                        session.setAttribute("katun", application.getAttribute("katun"));
                        session.setAttribute("baktun", application.getAttribute("baktun"));
                    }
                    %>
                    
        <div id="container">
            
            <div id="top">
                <%@include file="top.jspf" %>
            </div>
            
            <div id="content">
                <% if(valid){ %>
                    <h1>Login effettuata con successo.</h1>
                    <p>L'utente <%=maya%> ha effettuato con successo il login</p>
                <%} else {%>
                    <h1>Credenziali errate.</h1>
                    <p>Errore di login.</p>
                <%}%>
            </div>
            
            <div id="status">
                <%@include file="status.jspf" %>
            </div>
            
        </div>
        
        
    </body>
</html>
