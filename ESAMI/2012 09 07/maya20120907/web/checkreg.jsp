<%-- 
    Document   : index
    Created on : Feb 6, 2015, 9:05:47 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Maya</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        
                <%
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    String maya = (String)request.getParameter("maya");
                    Integer uinal = Integer.parseInt(request.getParameter("uinal"));
                    Integer tun = Integer.parseInt(request.getParameter("tun"));
                    Integer katun = Integer.parseInt(request.getParameter("katun"));
                    Integer baktun = Integer.parseInt(request.getParameter("baktun"));
                    
                    Boolean valid = true;
                    
                    if(uinal<0 || uinal>17 || 
                            tun<0 || tun>19 || 
                            katun<0 || katun > 19 ||
                            baktun<0 || baktun > 12){
                        valid = false;
                    }
                    
                    if(valid){
                        application.setAttribute("username", username);
                        application.setAttribute("password", password);
                        application.setAttribute("nomemaya", maya);
                        application.setAttribute("uinal", uinal);
                        application.setAttribute("tun", tun);
                        application.setAttribute("katun", katun);
                        application.setAttribute("baktun", baktun);
                    }
                    %>
        <div id="container">
            
            <div id="top">
                <%@include file="top.jspf" %>
            </div>
            
            <div id="content">
                <% if(valid){ %>
                    <h1>Registrazione avvenuta con successo</h1>
                    <p>Utente <%=maya%> registrato con successo.</p>
                <% } else { %>
                    <h1>Dati non validi</h1>
                    <p>Errore nei dati di registrazione. Riprovare.</p>
                <% } %>
            </div>
            
            <div id="status">
                <%@include file="status.jspf" %>
            </div>
            
        </div>
        
        
    </body>
</html>
