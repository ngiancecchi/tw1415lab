/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



window.onload = function () {
    document.getElementById("submit").onclick = function () {
        return check(this.form);
    };
};

function checkEmpty(string) {
    var regex = /^./;
    return regex.test(string);
}

function checkMayaName(string) {
    var regex = /^[^0-9]+$/;
    return regex.test(string);
}

function check(form) {

    var valid = true;

    if (!checkEmpty(form.username.value) || !checkEmpty(form.password.value) ||
            !checkEmpty(form.maya.value) || !checkEmpty(form.uinal.value) ||
            !checkEmpty(form.tun.value) || !checkEmpty(form.katun.value) ||
            !checkEmpty(form.baktun.value)){
        valid = false;
    }

    if (!checkMayaName(form.maya.value)) {
        valid = false;
    }

    if (valid === false) {
        alert("Ci sono campi errati");
        return false;
    }

    return true;


}