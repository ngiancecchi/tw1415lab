<%-- 
    Document   : index
    Created on : Feb 6, 2015, 9:05:47 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Maya</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script type='text/javascript' src='formcheck.js' ></script>
    </head>
    <body>
        
        <div id="container">
            
            <div id="top">
                <%@include file="top.jspf" %>
            </div>
            
            <div id="content">
                <h1>Login</h1>
                <form action="checklog.jsp" method="post">
                    
                    <div class="entry">
                        Username:
                        <input type="text" name="username" />
                    </div>
                    <div class="entry">
                        Password:
                        <input type="password" name="password" />
                    </div>
                    <div class="entry">
                        <input type="submit" name="submit" id="submit" value="Invia"/>
                    </div>
                    
                    
                </form>
            </div>
            
            <div id="status">
                <%@include file="status.jspf" %>
            </div>
            
        </div>
        
        
    </body>
</html>
