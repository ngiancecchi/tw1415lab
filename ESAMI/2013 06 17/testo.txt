La Festa della Repubblica Italiana viene celebrata il 2 giugno a ricordo della nascita della Repubblica.

Il 2 e il 3 giugno 1946 si tenne, infatti, il referendum istituzionale indetto a suffragio universale 
con il quale gli italiani venivano chiamati alle urne per esprimersi su quale forma di governo, 
monarchia o repubblica, dare al Paese, in seguito alla caduta del fascismo. Dopo 85 anni di regno, 
con 12.718.641 voti contro 10.718.502 l'Italia diventava repubblica e i monarchi di casa Savoia venivano esiliati.

Nel giugno del 1948 per la prima volta in Via dei Fori Imperiali a Roma ospit�; la parata militare in onore della Repubblica. 
