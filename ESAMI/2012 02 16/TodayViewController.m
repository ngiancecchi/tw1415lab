//
//  TodayViewController.m
//  Today
//
//  Created by Nicola Giancecchi on 02/02/15.
//  Copyright (c) 2015 Mr. APPs srl. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

@interface TodayViewController () <NCWidgetProviding>

@end

@implementation TodayViewController
static NSString *cellIdentifier = @"cellIdentifier";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData
    /*
    [[Database sharedDatabase] getProvidersAttiviOnlyArray:^(NSArray *data) {
        
        
        NSMutableArray *idArray=[[NSMutableArray alloc] init];
        
        for(Provider *p in data){
            [idArray addObject:p.idProvider];
        }
        
        NSString *string=[idArray componentsJoinedByString:@","];
        
        
        [[Webservice sharedWebservice] getNewsForProvider:string category:nil quantity:@(5) pageNumber:@(0) AndDone:^(BOOL completed, NSArray *response) {
            self.data=response;
            
        }];
    }];
    */
    completionHandler(NCUpdateResultNewData);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    News *n = self.data[indexPath.row];
    cell.textLabel.text = n.titolo;
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.data.count;
}


@end
