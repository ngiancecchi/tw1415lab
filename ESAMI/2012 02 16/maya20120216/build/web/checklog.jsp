<%-- 
    Document   : checklog
    Created on : Feb 2, 2015, 10:04:52 PM
    Author     : nicolagiancecchi
--%>

<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Il calendario Maya</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            
            <%@include file="top.jspf" %>
            <%@include file="navbar.jspf" %>
            
                <%
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    
                    Map<String,Object[]> users = (Map<String,Object[]>)application.getAttribute("users");
                    
                    if(users!=null){
                        Object[] userdata = users.get(username);
                        
                        if(userdata != null && userdata[0].equals(password)){
                           
                            session.setAttribute("username", username);
                            session.setAttribute("uinal", userdata[1]);
                            session.setAttribute("tun", userdata[2]);
                            session.setAttribute("katun", userdata[3]);
                            session.setAttribute("baktun", userdata[4]);
                    %>
                    
            <%@include file="status.jspf" %>
            
            <div id="content">
                
                
                <h1>Login effettuato con successo.</h1>
                <p>L'utente <%= username%> ha effettuato il login.</p>
                
                <%
                        }
                    } else {
                        %>
                  
            <div id="content">
                       
                <h1>Oops... Something went wrong!</h1>
                <p>Errore di login.</p>
                
                        <%
                    }
                        %>
            </div>
            
        </div>
    </body>
</html>
