<%-- 
    Document   : checkreg
    Created on : Feb 2, 2015, 10:04:45 PM
    Author     : nicolagiancecchi
--%>

<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Il calendario Maya</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            
            <%@include file="top.jspf" %>
            <%@include file="navbar.jspf" %>
            <%@include file="status.jspf" %>
            
            
            
            <div id="content">
                
                <%
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    Integer uinal = Integer.parseInt(request.getParameter("uinal"));
                    Integer tun = Integer.parseInt(request.getParameter("tun"));
                    Integer katun = Integer.parseInt(request.getParameter("katun"));
                    Integer baktun = Integer.parseInt(request.getParameter("baktun"));
                    
                    Boolean valid = true;
                    
                    if(uinal<0 || uinal>17)
                        valid=false;
                    
                    if(tun<0 || tun>19)
                        valid=false;
                    
                    if(katun<0 || tun>19)
                        valid=false;
                    
                    if(baktun<0 || baktun>12)
                        valid=false;
                    
                    if(valid){
                        Map<String,Object[]> users = (Map<String,Object[]>)application.getAttribute("users");
                        if(users==null){
                            users = new HashMap<String,Object[]>();
                            application.setAttribute("users", users);
                        }
                        
                        users.put(username, new Object[]{password,uinal,tun,katun,baktun});

                        session.invalidate();
                
                    %>
                
                <h1>Registrazione avvenuta con successo.</h1>
                <p>Utente <%= username%> registrato con successo.</p>
                <p>Puoi <a href="login.jsp">eseguire la login</a> oppure 
                    <a href="index.jsp">tornare alla home page</a></p>
                
                <%
                    } else {
                        
                        %>
                         
                <h1>Oops... Something went wrong!</h1>
                <p>Errore nei dati di registrazione.</p>
                
                        <%
                    }
                        %>
            </div>
            
        </div>
    </body>
</html>
