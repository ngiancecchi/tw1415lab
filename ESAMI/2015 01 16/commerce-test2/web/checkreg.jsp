<%-- 
    Document   : index
    Created on : Feb 9, 2015, 3:26:27 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TW-Commerce</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        
                <%
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    String nome = (String)request.getParameter("nome");
                    String cognome = (String)request.getParameter("cognome");
                    String pagamento = (String)request.getParameter("pagamento");
                    
                    application.setAttribute("username", username);
                    application.setAttribute("password", password);
                    application.setAttribute("nome", nome);
                    application.setAttribute("cognome", cognome);
                    application.setAttribute("pagamento", pagamento);
                    
                    session.setAttribute("nome", null);
                    session.setAttribute("cognome", null);
                    session.setAttribute("pagamento", null);
                %>
        <div id="main">
            <header>
                <%@include file="header.jspf" %>
            </header>

            <div id='loginmessage'>
                <h1>Utente <%=nome%> <%=cognome%> registrato con successo.</h1>
            
            </div>
                
            <footer> 
                <%@include file="footer.jspf" %>
            </footer>
        </div>
    </body>
</html>
