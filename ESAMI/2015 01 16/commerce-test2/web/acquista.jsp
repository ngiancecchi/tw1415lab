<%-- 
    Document   : index
    Created on : Feb 9, 2015, 3:26:27 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TW-Commerce</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="main">
            <header>
                <%@include file="header.jspf" %>
            </header>

            <div id='loginmessage'>
                <%
                    String payment = (String) session.getAttribute("pagamento");

                    if (payment == null) {
                %>
                <h1>Nessun utente loggato, impossibile eseguire l'acquisto</h1>
                <%
                        } else { 
                %>
                <h1>Grazie per l'acquisto. La modalit&agrave; scelta per il 
                    pagamento &egrave;: <span class='bold'><%= payment %></span> </h1>
                <%
                    }
                %>
            
            </div>
                
            <footer> 
                <%@include file="footer.jspf" %>
            </footer>
        </div>
    </body>
</html>
