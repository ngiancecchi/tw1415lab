<%-- 
    Document   : index
    Created on : Feb 9, 2015, 3:26:27 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TW-Commerce</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="main">
            <header>
                <%@include file="header.jspf" %>
            </header>

            <form action="checklog.jsp" method="post">
                <div class="entry">
                    Username:
                    <input type="text" name="username" placeholder="username" />
                </div>
                <div class="entry">
                    Password:
                    <input type="password" name="password" placeholder="password" />
                </div>
                
                <div class="entry">
                    <input type="submit" id="submit" value="login" />
                </div>
                
            </form>

            <footer> 
                <%@include file="footer.jspf" %>
            </footer>
        </div>
    </body>
</html>
