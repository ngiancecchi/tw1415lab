/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


window.onload = function(){
    document.getElementById("submit").onclick = function(){
        return check(this.form);
    };
};

function checkEmpty(string){
    var regexp = /^./;
    return regexp.test(string);
}

function checkPassword(string){
    var regexp = /^[a-zA-Z0-9]{5,}$/;
    return regexp.test(string);
}

function check(form){
    var valid = true;
    
    if(!checkEmpty(form.username.value) || !checkEmpty(form.password.value)
            || !checkEmpty(form.nome.value) || !checkEmpty(form.cognome.value)){
        valid = false;
    }
    
    if(!checkPassword(form.password.value)){
        valid = false;
    }
    
    if(!valid){
        alert("Ci sono controlli errati.");
    }
    
    return valid;
}