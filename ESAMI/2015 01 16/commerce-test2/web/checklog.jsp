<%-- 
    Document   : index
    Created on : Feb 9, 2015, 3:26:27 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TW-Commerce</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <%
            Boolean valid = false;

            String username = (String) request.getParameter("username");
            String password = (String) request.getParameter("password");

            String appUN = (String) application.getAttribute("username");
            String appPW = (String) application.getAttribute("password");

            String appNome = (String) application.getAttribute("nome");
            String appCognome = (String) application.getAttribute("cognome");
            String appPagamento = (String) application.getAttribute("pagamento");

            if (appUN != null && appPW != null
                    && username.equals(appUN) && password.equals(appPW)) {
                valid = true;
                session.setAttribute("nome", appNome);
                session.setAttribute("cognome", appCognome);
                session.setAttribute("pagamento", appPagamento);
            }

        %>

        <div id="main">
            <header>
                <%@include file="header.jspf" %>
            </header>

            <div id='loginmessage'>
                <% if (valid) {%>
                <h1>L'utente <%=session.getAttribute("nome")%> 
                    <%=session.getAttribute("cognome")%> ha effettuato 
                    la login.</h1>
                    <% } else { %>
                <h1>Errore di login</h1>
                <% }%>

            </div>

            <footer> 
                <%@include file="footer.jspf" %>
            </footer>
        </div>
    </body>
</html>
