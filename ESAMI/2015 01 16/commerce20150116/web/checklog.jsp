<%-- 
    Document   : index
    Created on : Feb 3, 2015, 3:15:56 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>TW-Commerce</title>
    </head>
    <body>
        
        <div id="main">
                <%
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    
                    String usernameAppl = (String)application.getAttribute("username");
                    String passwordAppl = (String)application.getAttribute("password");
                    String nomeAppl = (String)application.getAttribute("nome");
                    String cognomeAppl = (String)application.getAttribute("cognome");
                    String pagamento = (String)application.getAttribute("pagamento");
                    
                    if(usernameAppl.equals(username) && passwordAppl.equals(password)){
                        session.setAttribute("username", username);
                        session.setAttribute("pagamento", pagamento);
                        session.setAttribute("nome",nomeAppl);
                        session.setAttribute("cognome",cognomeAppl);
                    %>
                    
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <div id="buyingMessage">
                    L'utente <%= nomeAppl %> <%= cognomeAppl %> ha effettuato il login.
                    <%
                    } else {
                        %>
                        
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <div id="buyingMessage">
                        Errore di login.
                        <%
                    }
                    %>
            </div>    
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        </div>
        
        
    </body>
</html>
