<%-- 
    Document   : index
    Created on : Feb 3, 2015, 3:15:56 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>TW-Commerce</title>
    </head>
    <body>
        
        <div id="main">
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            
            <div id='buyingMessage'>
                
                <%
                    String pagamento = (String)session.getAttribute("pagamento");
                    if(pagamento!=null){
                        
                    %>
                    
                    Grazie per l'acquisto. La modalità scelta 
                        per il pagamento è: <span class='bold'><%= pagamento%></span>
                    
                    <%
                    } else {
                    %>
                    
                    Nessun utente loggato, impossibile eseguire l'acquisto.
                    
                    <%
                    } 
                    %>
                    
            </div>
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        </div>
        
        
    </body>
</html>
