<%-- 
    Document   : index
    Created on : Feb 3, 2015, 3:15:56 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script type='text/javascript' src="formcheck.js"></script>
        <title>TW-Commerce</title>
    </head>
    <body>
        
        <div id="main">
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <form action='checkreg.jsp' method='post' >
                
                <div class="entry">
                    <p class="label">Username:</p>
                    <input type="text" name="username" placeholder="username">
                </div>
                
                <div class="entry">
                    <p class="label">Password:</p>
                    <input type="password" name="password" placeholder="password">
                </div>
                
                <div class="entry">
                    <p class="label">Nome:</p>
                    <input type="text" name="nome" placeholder="nome">
                </div>
                
                <div class="entry">
                    <p class="label">Cognome:</p>
                    <input type="text" name="cognome" placeholder="cognome">
                </div>
                
                Modalit&agrave; di pagamento:
                
                <p class="radiolabel">
                bonifico
                <input type="radio" name="pagamento" value="bonifico" checked="checked"/>
                </p>
                
                <p class="radiolabel">
                contrassegno
                <input type="radio" name="pagamento" value="contrassegno"/>
                </p>
                
                <p id="submitparag">
                <input type="submit" name="submit" id="submit" value="Registrazione" />
                </p>
                
            </form>
            
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        </div>
        
        
    </body>
</html>
