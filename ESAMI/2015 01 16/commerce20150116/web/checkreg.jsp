<%-- 
    Document   : index
    Created on : Feb 3, 2015, 3:15:56 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>TW-Commerce</title>
    </head>
    <body>
        
        <div id="main">
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <div id="buyingMessage">
                <%
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    String nome = (String)request.getParameter("nome");
                    String cognome = (String)request.getParameter("cognome");
                    String pagamento = (String)request.getParameter("pagamento");
                    
                    application.setAttribute("username", username);
                    application.setAttribute("password", password);
                    application.setAttribute("nome", nome);
                    application.setAttribute("cognome", cognome);
                    application.setAttribute("pagamento", pagamento);
                    
                    session.invalidate();
                    %>
                    Utente <%= nome%> <%= cognome %> registrato con successo.
                    
            </div>    
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        </div>
        
        
    </body>
</html>
