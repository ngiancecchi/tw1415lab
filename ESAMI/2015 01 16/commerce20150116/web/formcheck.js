/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.onload = function(){
    document.getElementById("submit").onclick = function(){
        return check(this.form);
    };
};

function checkField(string){
    var regexp = /^./;
    return regexp.test(string);
}

function checkPassword(string){
    var regexp = /^[a-zA-Z0-9]{5,}$/;
    return regexp.test(string);
}

function check(form){
    
    var valid = true;
    
    if(!checkField(form.username.value)){
        valid=false;
    }
    
    if(!checkField(form.password.value) || !checkPassword(form.password.value)){
        valid=false;
    }
    
    if(!checkField(form.nome.value)){
        valid=false;
    }
    
    if(!checkField(form.cognome.value)){
        valid=false;
    }
    
    if(valid===false){
        alert("Ci sono errori di compilazione");
        return false;
    }
    
    return true;
    
    
}

/*
 * Dotare la pagina registrazione.jsp di uno script javascript unobtrusive che 
 * verifichi che tutti i controlli della rispettiva form siano costituiti da valori 
 * non vuoti. Controllare inoltre che la password sia formata da almeno 5 caratteri 
 * alfanumerici (caratteri che siano lettere maiuscole o minuscole o numeri). 
 * In caso l’utente prema il bottone di sottomissione della form con un qualche 
 * controllo errato si deve aprire un box di pop-up con un bottone “ok” e con la 
 * scritta “Ci sono controlli errati.” e (anche dopo che l’utente ha premuto “ok”) 
 * l’invio della form non deve avvenire. 
 */