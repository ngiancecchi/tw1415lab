<%-- 
    Document   : index
    Created on : Feb 3, 2015, 3:15:56 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>TW-Commerce</title>
    </head>
    <body>
        
        <div id="main">
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <form action='checklog.jsp' method='post' >
                
                <div class="entry">
                    <p class="label">Username:</p>
                    <input type="text" name="username" placeholder="username">
                </div>
                
                <div class="entry">
                    <p class="label">Password:</p>
                    <input type="password" name="password" placeholder="password">
                </div>
                
                <p id="submit">
                <input type="submit" name="submit" id="submit" value="Login" />
                </p>
                
            </form>
            
            
            <footer>
                <%@include file="footer.jspf" %>
            </footer>
        </div>
        
        
    </body>
</html>
