<%-- 
    Document   : index
    Created on : Feb 6, 2015, 4:05:04 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Agenda di Luglio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        
                <%
                    
                    
                    String nome = (String)request.getParameter("nome");
                    String cognome = (String)request.getParameter("cognome");
                    String natoa = (String)request.getParameter("natoa");
                    String provincia = (String)request.getParameter("provincia");
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    
                    application.setAttribute("nome", nome);
                    application.setAttribute("cognome", cognome);
                    application.setAttribute("natoa", natoa);
                    application.setAttribute("provincia", provincia);
                    application.setAttribute("username", username);
                    application.setAttribute("password", password);
                    
                    %>
        <div id="container">
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <nav>
                <%@include file="nav.jspf" %>
            </nav>
            
                <%@include file="status.jspf" %>
            
            <div id="logincontainer">
                
                
                    <h2>Registrazione avvenuta con successo</h2>
                    
                    <p>Utente <%=nome%> <%=cognome%> registrato con successo.</p>
                
            </div>
                    
                    
            <footer>
                
            </footer>
            
        </div>
        
    </body>
</html>
