/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.onload = function(){
    document.getElementById("submit").onclick = function(){
        return check(this.form);
    };
};

function checkEmpty(string){
    var regexp = /^./;
    return regexp.test(string);
}

function checkCity(string){
    var regexp = /^([A-Z]{1})([a-z ]+)$/;
    return regexp.test(string);
}

function checkProvince(string){
    var regexp = /^[A-Z]{2}$/;
    return regexp.test(string);
}


function check(form){
    
    var valid = true;
    
    if(!checkEmpty(form.username.value)){
        valid = false;
    }
    if(!checkEmpty(form.password.value)){
        valid = false;
    }
    if(!checkEmpty(form.nome.value)){
        valid = false;
    }
    if(!checkEmpty(form.cognome.value)){
        valid = false;
    }
    if(!checkEmpty(form.natoa.value) || !checkCity(form.natoa.value)){
        valid = false;
    }
    if(!checkEmpty(form.provincia.value) || !checkProvince(form.provincia.value)){
        valid = false;
    }
    
    if(valid===false){
        alert("Ci sono controlli errati");
        return false;
    }
    
    return true;
    
}