<%-- 
    Document   : index
    Created on : Feb 6, 2015, 4:05:04 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Agenda di Luglio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script type='text/javascript' src='formcheck.js'></script>
    </head>
    <body>
        <div id="container">
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <nav>
                <%@include file="nav.jspf" %>
            </nav>
            
                <%@include file="status.jspf" %>
            
            <div id="logincontainer">
                <form method="post" action="checkreg.jsp">
                    
                    <div class="entry">
                        Nome:
                        <input type="text" name="nome" placeholder="nome" size="25"/>
                    </div>
                    
                    <div class="entry">
                        Cognome:
                        <input type="text" name="cognome" placeholder="cognome" size="25"/>
                    </div>
                    
                    <div class="entry">
                        Nato a:
                        <input type="text" name="natoa" placeholder="nato a" size="25"/>
                    </div>
                    
                    <div class="entry">
                        Provincia:
                        <input type="text" name="provincia" placeholder="provincia" size="25"/>
                    </div>
                    
                    <div class="entry">
                        Username:
                        <input type="text" name="username" placeholder="username" size="25"/>
                    </div>
                    
                    <div class="entry">
                        Password:
                        <input type="password" name="password" placeholder="password" size="25"/>
                    </div>
                    
                    <div class="entry">
                        <input type="submit" id="submit" name="submit" value="Invia" />
                    </div>
                    
                </form>
                    
            </div>
            <footer>
                
            </footer>
            
        </div>
        
    </body>
</html>
