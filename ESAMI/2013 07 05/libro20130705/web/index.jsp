<%-- 
    Document   : index
    Created on : Feb 6, 2015, 4:05:04 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Agenda di Luglio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <nav>
                <%@include file="nav.jspf" %>
            </nav>
            
                <%@include file="status.jspf" %>
            
            <article>
                <div id="table">
                    <div class="row">
                        <div class="headercell">Lun</div>
                        <div class="headercell">Mar</div>
                        <div class="headercell">Mer</div>
                        <div class="headercell">Gio</div>
                        <div class="headercell">Ven</div>
                        <div class="headercell">Sab</div>
                        <div class="headercell">Dom</div>
                    </div>
                    
                    <div class="row">
                        <div class="cell">1</div>
                        <div class="cell">2</div>
                        <div class="cell">3</div>
                        <div class="cell">4</div>
                        <div class="today">5</div>
                        <div class="cell">6</div>
                        <div class="cell"><span class="sunday">7</span></div>
                    </div>
                    
                    <div class="row">
                        <div class="cell">8</div>
                        <div class="cell">9</div>
                        <div class="cell">10</div>
                        <div class="cell">11</div>
                        <div class="cell">12</div>
                        <div class="cell">13</div>
                        <div class="cell"><span class="sunday">14</span></div>
                    </div>
                    
                    <div class="row">
                        <div class="cell">15</div>
                        <div class="cell">16</div>
                        <div class="cell">17</div>
                        <div class="events">18</div>
                        <div class="cell">19</div>
                        <div class="cell">20</div>
                        <div class="cell"><span class="sunday">21</span></div>
                    </div>
                    
                    <div class="row">
                        <div class="cell">22</div>
                        <div class="cell">23</div>
                        <div class="cell">24</div>
                        <div class="cell">25</div>
                        <div class="cell">26</div>
                        <div class="cell">27</div>
                        <div class="events"><span class="sunday">28</span></div>
                    </div>
                    
                    <div class="row">
                        <div class="cell">29</div>
                        <div class="cell">30</div>
                        <div class="events">31</div>
                    </div>
                </div>
                
                <aside>
                    <span class="underlined">Il 5 luglio del 1903 parte il primo 
                        <span class="bolditalic">Tour de France</span></span>, 
                        uno dei tre grandi giri maschili di ciclismo su strada. 
                    A partire da quella prima data la corsa si &egrave; svolta ogni 
                    anno, a eccezione dei periodi delle due guerre mondiali. 
                    Dal 1984 &egrave; stato affiancato dal Giro di Francia femminile.
                </aside>
            </article>
            
            <section>
                <p>5 luglio 1687</p>
                <ul>
                    <li>
                        Viene pubblicato il Philosophiae Naturalis Principia 
                        Mathematica di Isaac Newton
                    </li>
                </ul>
                
            </section>
            
            <footer>
                
            </footer>
            
        </div>
        
    </body>
</html>
