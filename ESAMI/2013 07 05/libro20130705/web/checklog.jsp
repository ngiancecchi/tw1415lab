<%-- 
    Document   : index
    Created on : Feb 6, 2015, 4:05:04 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Agenda di Luglio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        
                        
                <%
                    Boolean logged = false;
                    String username = (String)request.getParameter("username");
                    String password = (String)request.getParameter("password");
                    
                    String unApp = (String)application.getAttribute("username");
                    String pwApp = (String)application.getAttribute("password");
                    
                        String nome = (String)application.getAttribute("nome");
                        String cognome = (String)application.getAttribute("cognome");
                    
                    if(username != null && password != null &&
                            unApp != null && pwApp != null){
                        if(username.equals(unApp) && password.equals(pwApp)){
                            logged = true;
                    
                        String natoa = (String)application.getAttribute("natoa");
                        String provincia = (String)application.getAttribute("provincia");
                        
                        session.setAttribute("nome", nome);
                        session.setAttribute("cognome", cognome);
                        session.setAttribute("natoa", natoa);
                        session.setAttribute("provincia", provincia);
                        session.setAttribute("username", username);
                        }
                    }
                    
                    %>
                
                    
        <div id="container">
            <header>
                <%@include file="header.jspf" %>
            </header>
            
            <nav>
                <%@include file="nav.jspf" %>
            </nav>
            
                <%@include file="status.jspf" %>
            
            <div id="logincontainer">

                <%
                    if(logged){
                    %>
                    <h2>Login effettuata con successo</h2>
                    
                    <p>L'utente <%=nome%> <%=cognome%> ha effettuato la login.</p>
                    
                    <% } else { %>
                    <h2>Credenziali errate</h2>
                    
                    <p>Errore di login.</p>
                    
                    <%}%>
                
            </div>
                    
                    
            <footer>
                
            </footer>
            
        </div>
        
    </body>
</html>
