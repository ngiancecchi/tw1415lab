<%-- 
    Document   : index
    Created on : Feb 7, 2015, 10:00:01 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Il Calendario Maya</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script type='text/javascript' src='formcheck.js' ></script>
    </head>
    <body>
        
        <div id="container">
            
            <div id="top">
                <%@include file="top.jspf" %>
            </div>
            
            <div id="navbar">
                <%@include file="navbar.jspf" %>
            </div>
            
            <div id="status">
                <%@include file="status.jspf" %>
            </div>
            
            <div id="content">
                <h1>Login</h1>
                
                <form action="checklog.jsp" method="post">
                    <div class="entry">
                        Username:
                        <input type="text" name="username" placeholder="username" />
                    </div>
                    <div class="entry">
                        Password:
                        <input type="password" name="password" placeholder="password" />
                    </div>
                    <div class="entry">
                        <input type="submit" name="submit" id="submit" value="Do the Harlem Login" />
                    </div>
                    
                    
                </form>
            </div>
            
        </div>
        
    </body>
</html>
