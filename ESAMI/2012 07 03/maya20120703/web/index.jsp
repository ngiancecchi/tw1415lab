<%-- 
    Document   : index
    Created on : Feb 7, 2015, 10:00:01 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Il Calendario Maya</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        
        <div id="container">
            
            <div id="top">
                <%@include file="top.jspf" %>
            </div>
            
            <div id="navbar">
                <%@include file="navbar.jspf" %>
            </div>
            
            <div id="status">
                <%@include file="status.jspf" %>
            </div>
            
            <div id="content">
                <p>La civilt&agrave; maya fior&igrave; nella zona del Centro America 
                che si estende attualmente dal sud della Penisola dello Yucat&aacute;n 
                fino all'Honduras e El Salvador passando per Guatemala e Belize. 
                Gli archeologi dividono questa vasta area in due regioni: una a 
                sud denominata "terre alte", costituite dal sistema montuoso 
                presente sul territorio guatemalteco e una regione a nord o 
                "terre basse", che comprende la foresta tropicale del Guatemala e 
                Belize del nord e le zone pi&ugrave; aride della penisola dello 
                Yucat&aacute;n. Il famoso Calendario Maya era composto da 5 periodi:</p>
                <ul>
                    <li>20 giorni (prima cifra): uinal</li>
                    <li>360 giorni (seconda cifra, 18&times;20 = 360): tun</li>
                    <li>7200 giorni (terza cifra, 20&times;360 = 7200): k'atun</li>
                    <li>144000 giorni (quarta cifra, 20&times;7200 = 144000): b'ak'tun</li>
                    <li>la quinta cifra si ripete dopo il ciclo completo di 1872000 giorni 
                        (13&times;144000 = 1872000).</li>
                </ul>
                <p>Il ciclo attualmente in corso, che secondo la mitologia maya 
                    &egrave; il quarto, &egrave; iniziato il 11 agosto 3114 a.C. 
                    ed &egrave; molto vicino al termine: il nuovo ciclo inizier&agrave; 
                    il 21 dicembre 2012. "Per gli antichi Maya, si doveva tenere 
                    un'enorme celebrazione alla fine di un intero ciclo" dice 
                    Sandra Noble, la executive director della Foundation for the 
                    Advancement of Mesoamerican Studies, Inc. a Crystal River in 
                    Florida. "Rendere il 21 dicembre 2012 come un Giorno del 
                    giudizio o un momento di cambiamento cosmico" dice "&egrave; 
                    una completa invenzione e una possibilit&agrave; per molte 
                    persone di fare profitto".</p>
            </div>
            
        </div>
        
    </body>
</html>
