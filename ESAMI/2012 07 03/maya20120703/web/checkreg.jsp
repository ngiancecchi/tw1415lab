<%-- 
    Document   : index
    Created on : Feb 7, 2015, 10:00:01 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Il Calendario Maya</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <%
            
            String username = (String)request.getParameter("username");
            String password = (String)request.getParameter("password");
            String nomemaya = (String)request.getParameter("nomemaya");
            String provenienza = (String)request.getParameter("provenienza");
            
            application.setAttribute("username", username);
            application.setAttribute("password", password);
            application.setAttribute("nomemaya", nomemaya);
            application.setAttribute("provenienza", provenienza);
            
            %>
        
        <div id="container">
            
            <div id="top">
                <%@include file="top.jspf" %>
            </div>
            
            <div id="navbar">
                <%@include file="navbar.jspf" %>
            </div>
            
            <div id="status">
                <%@include file="status.jspf" %>
            </div>
            
            <div id="content">
                <h1>Registrazione effettuata con successo</h1>
                <p class="avviso">Utente <%=nomemaya%> di provenienza <%=provenienza%> 
                    registrato con successo.</p>
            </div>
            
        </div>
        
    </body>
</html>
