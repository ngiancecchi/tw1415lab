<%-- 
    Document   : index
    Created on : Feb 7, 2015, 10:00:01 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Il Calendario Maya</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <%
            String username = (String)request.getParameter("username");
            String password = (String)request.getParameter("password");
            Boolean logged = false;
            
            String unApp = (String)application.getAttribute("username");
            String pwApp = (String)application.getAttribute("password");
            
            if(unApp != null && username.equals(unApp) &&
                    pwApp != null && password.equals(pwApp)){
                logged = true;
                
                session.setAttribute("username", username);
                session.setAttribute("nomemaya", application.getAttribute("nomemaya"));
                session.setAttribute("provenienza", application.getAttribute("provenienza"));
            }
            
            %>
        
        <div id="container">
            
            <div id="top">
                <%@include file="top.jspf" %>
            </div>
            
            <div id="navbar">
                <%@include file="navbar.jspf" %>
            </div>
            
            <div id="status">
                <%@include file="status.jspf" %>
            </div>
            
            <div id="content">
                <% if (logged){ %>
                    <h1>Login effettuato con successo</h1>
                    <p class="avviso">L'utente 
                        <%= session.getAttribute("provenienza") %> ha effettuato 
                        la login.</p>
                <% } else { %>
                    <h1>Credenziali errate</h1>
                    <p class='avviso'>Errore di login</p>
                <%}%>
            </div>
            
        </div>
        
    </body>
</html>
