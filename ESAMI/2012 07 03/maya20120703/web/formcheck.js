/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


window.onload = function(){
    document.getElementById("submit").onclick = function(){
        return check(this.form);
    };
};

function checkEmpty(string){
    var regexp = /^./;
    return regexp.test(string);
}

function check(form){
    
    var valid = true;
    
    if(!checkEmpty(form.username.value) || !checkEmpty(form.password.value)
            || !checkEmpty(form.nomemaya.value)){
            valid = false;
    }
    
    if(valid===false){
        alert("Ci sono controlli errati");
        return false;
    }
    
    return true;
}