<%-- 
    Document   : index
    Created on : Feb 7, 2015, 10:00:01 AM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Il Calendario Maya</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script type='text/javascript' src='formcheck.js' ></script>
    </head>
    <body>
        
        <div id="container">
            
            <div id="top">
                <%@include file="top.jspf" %>
            </div>
            
            <div id="navbar">
                <%@include file="navbar.jspf" %>
            </div>
            
            <div id="status">
                <%@include file="status.jspf" %>
            </div>
            
            <div id="content">
                <h1>Registrazione</h1>
                
                <form action="checkreg.jsp" method="post">
                    <div class="entry">
                        Username:
                        <input type="text" name="username" placeholder="username" />
                    </div>
                    <div class="entry">
                        Password:
                        <input type="password" name="password" placeholder="password" />
                    </div>
                    <div class="entry">
                        Nome Maya:
                        <input type="text" name="nomemaya" placeholder="nome Maya" />
                    </div>
                    <div class="entry">
                        Provenienza: Terra Alta
                        <input type="radio" name="provenienza" value="Terra Alta" checked="true" /><br/>
                        Terra Bassa <input type="radio" name="provenienza" value="Terra Bassa" />
                        
                    </div>
                    <div class="entry">
                        <input type="submit" name="submit" id="submit" value="Registrati" />
                    </div>
                    
                    
                </form>
            </div>
            
        </div>
        
    </body>
</html>
