<%-- 
    Document   : index
    Created on : Feb 2, 2015, 5:38:12 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agenda di Febbraio</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            
            <%@include file="header.jspf" %>
            
            <%@include file="nav.jspf" %>
                
            <article>
                
                <div id="table">
                </div>
                
                <section>
                    12 febbraio 1912<br/>
- La Cina adotta il Calendario Gregoriano.<br/>
- L'ultimo Imperatore Cinese, Pu Yi abdica in favore della Repubblica.<br/>
                </section>
                
                <aside>
                    <p><span class="underlined"> Charles Robert Darwin (Shrewsbury, <span class="bolditalic">12 febbraio 1809 - Londra, 19 aprile 1882</span>)</span> &egrave; stato un naturalista britannico, 
celebre per aver formulato la teoria dell'evoluzione delle specie animali e vegetali per selezione naturale agente sulla variabilit&agrave; dei caratteri 
(origine delle specie), per aver teorizzato la discendenza di tutti i primati (uomo compreso) da un antenato comune (origine dell'uomo) 
e per aver teorizzato la possibile esistenza di un antenato comune a tutte le specie viventi.</p>

<p>Pubblic&ograve; la sua teoria sull'evoluzione delle specie nel libro L'origine delle specie (1859), 
    che &egrave; rimasto il suo lavoro pi&ugrave; noto. </p>

<p>Raccolse molti dei dati su cui bas&ograve; la sua teoria durante un viaggio intorno al mondo sulla nave HMS Beagle, 
    e in particolare durante la sua sosta alle Isole Gal&agrave;pagos.</p>

                </aside>
            </article>
            
            <footer>
            </footer>
            
        </div>
    </body>
</html>
