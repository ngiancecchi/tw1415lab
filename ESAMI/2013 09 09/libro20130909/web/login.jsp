<%-- 
    Document   : index
    Created on : Feb 5, 2015, 2:55:13 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agenda di Settembre</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script type="text/javascript" src="formcheck.js" /></script>
    </head>
    <body>
        <div id="container">
            <%@include file="header.jspf" %>
            <%@include file="nav.jspf" %>
            
            <form action="checklog.jsp" method="post">

                <div class="entry">
                    Username:
                    <input type="text" name="username" placeholder="username" size="25" />
                </div>
                
                <div class="entry">
                    Password:
                    <input type="password" name="password" placeholder="password" size="25" />
                </div>
                
                <div class="entry">
                    <input type="submit" id="submit" name="submit" value="Invia" />
                </div>
            </form>
            
            
            <footer>

            </footer>
        </div>
    </body>
</html>
