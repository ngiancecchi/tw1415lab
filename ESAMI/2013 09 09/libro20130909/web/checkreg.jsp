<%-- 
    Document   : index
    Created on : Feb 5, 2015, 2:55:13 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agenda di Settembre</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script type="text/javascript" src="formcheck.js" /></script>
    </head>
    <body>
        <%
            String nome = (String)request.getParameter("nome");
            String cognome = (String)request.getParameter("cognome");
            String prefisso = (String)request.getParameter("prefisso");
            String telefono = (String)request.getParameter("telefono");
            String username = (String)request.getParameter("username");
            String password = (String)request.getParameter("password");
            
            application.setAttribute("nome", nome);
            application.setAttribute("cognome", cognome);
            application.setAttribute("prefisso", prefisso);
            application.setAttribute("telefono", telefono);
            application.setAttribute("username", username);
            application.setAttribute("password", password);
            
            %>
        <div id="container">
            <%@include file="header.jspf" %>
            <%@include file="nav.jspf" %>
            
            <div id="logininfo">
                <h1>Registrazione effettuata correttamente</h1>
                <p>Utente <%= nome%> <%=cognome%> registrato con successo.</p>
            </div>
            
            <footer>

            </footer>
        </div>
    </body>
</html>
