/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

window.onload = function(){
    document.getElementById("submit").onclick = function(){
        return check(this.form);
    };
};

function checkEmpty(string){
    var regexp = /^./;
    return regexp.test(string);
}

function checkPrefix(string){
    var regexp = /^[+][0-9]{1,3}$/;
    return regexp.test(string);
}

function checkPhone(string){
    var regexp = /^[0-9]{5,10}$/;
    return regexp.test(string);
}

function check(form){
    
    var valid = true;
    
    if(!checkEmpty(form.username.value) || !checkEmpty(form.password.value)
            || !checkEmpty(form.nome.value) || !checkEmpty(form.cognome.value) 
            || !checkEmpty(form.prefisso.value) || !checkEmpty(form.telefono.value) ){
        valid = false;
    }
    
    if(!checkPrefix(form.prefisso.value)){
        valid = false;
    }
    
    if(!checkPhone(form.telefono.value)){
        valid = false;
    }
    
    if(valid===false){
        alert('Ci sono controlli errati');
        return false;
    }
    
    return true;
}

/*
 * verifichi che che tutti i controlli della form siano costituiti da valori non 
 * vuoti e cheil prefisso sia composto da un “+”seguito da 1,2 o 3 cifre e il telefono sia 
 * un numero composto da minimo 5 e massimo 10 cifre. In caso l’utente prema il 
 * bottone di sottomissione della form con un qualche controllo errato si deve 
 * aprire un box di pop-up con un bottone “ok” e con la scritta “Ci sono 
 * controlli errati.” e (anche dopo che l’utente ha premuto “ok”) l’invio 
 * della form non deve avvenire. 
 */

