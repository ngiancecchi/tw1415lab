<%-- 
    Document   : index
    Created on : Feb 5, 2015, 2:55:13 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agenda di Settembre</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            <%@include file="header.jspf" %>
            <%@include file="nav.jspf" %>
            

            <article>
                <!-- tabella here flottata a sx -->
                <div id="table">

                    <div id="header">
                        <div class="headercell">Lun</div>
                        <div class="headercell">Mar</div>
                        <div class="headercell">Mer</div>
                        <div class="headercell">Gio</div>
                        <div class="headercell">Ven</div>
                        <div class="headercell">Sab</div>
                        <div class="headercell">Dom</div>
                    </div>

                    <%
                        for (int i = 1; i < 7; i++) {
                    %>
                    <div class="row">
                        <%                            
                            for (int k = 1; k < 8; k++) {
                                Integer day = k + ((i - 1) * 7) - 6;

                                if (day == 9) {
                        %>
                                <div class="today"> 
                            <%  } else if(day == 22 || day == 27){
                                            %>
                                <div class="events">
                             <% } else {
                                            %>
                                <div class="cell">
                             <%
                                }
                                
                                if(day>0&&day<31){
                                    if(k==7){
                                        %>
                                        <span class="sunday">
                                            <%= day %> 
                                        </span>
                                        <%
                                    } else {
                                        %>
                                    <%= day %>
                                <%
                                    } 
                                }
                            %>
                            </div> 
                            <%
                            }
                            %>
                        </div>

                        <%
                            }

                        %>
                    </div>

                    <aside>
                        <p><span class="underlined">Luigi Galvani (Bologna, 9 settembre 1737 
                                Bologna, - 4 dicembre 1798)</span> &egrave; stato un fisiologo, 
                            fisico e anatomista italiano. Luigi Galvani &egrave; oggi 
                            ricordato come lo scopritore dell'elettricit&agrave; biologica 
                            e per diverse applicazioni della metamorfosi, come la cella 
                            elettrochimica, il galvanometro e la galvanizzazione.
                            Gli studi per i quali Galvani &egrave; maggiormente ricordato 
                            riguardano la cosiddetta elettricit&agrave; animale. Nel 1791 
                            pubblic&ograve; il De viribus electricitatis in motu musculari 
                            commentarius, opera in cui esponeva le sue teorie riguardanti 
                            l'argomento, teorie frutto di lunghi studi e indagini 
                            sperimentali.</p>
                        <p>Galvani ipotizz&ograve; quindi una relazione fra elettricit&agrave; 
                            e vita, e decise di continuare a condurre esperimenti su rane, 
                            osservando il movimento dei muscoli in relazione alla carica 
                            elettrostatica con cui venivano toccati. Galvani ipotizz&ograve; 
                            l'esistenza di una elettricit&agrave; intrinseca all'animale 
                            che produce la contrazione dei muscoli. Per Galvani, il 
                            muscolo della rana, oltre ad essere un rivelatore sensibilissimo 
                            era dunque un serbatoio di elettricit&agrave;.</p>
                    </aside>

                    <section>
                        <p>9 settembre 1956</p>
                        <ul>
                            <li>Elvis Presley appare per la prima volta al The Ed Sullivan Show</li>
                        </ul>
                    </section>
            </article>

            <footer>

            </footer>
        </div>
    </body>
</html>
