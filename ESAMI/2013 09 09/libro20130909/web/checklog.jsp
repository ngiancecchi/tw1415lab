<%-- 
    Document   : index
    Created on : Feb 5, 2015, 2:55:13 PM
    Author     : nicolagiancecchi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agenda di Settembre</title>
        <link rel="stylesheet" href="style.css" type="text/css" />
        <script type="text/javascript" src="formcheck.js" /></script>
    </head>
    <body>
        <%
            Boolean logged = false;
            String username = (String)request.getParameter("username");
            String password = (String)request.getParameter("password");
            
            String unApp = (String)application.getAttribute("username");
            String unPw = (String)application.getAttribute("password");
            
            if(username != null && password != null && unApp != null && unPw != null){
                if(username.equals(unApp) && password.equals(unPw)){
                    logged = true;
                    String nome = (String)application.getAttribute("nome");
                    String cognome = (String)application.getAttribute("cognome");
                    String prefisso = (String)application.getAttribute("prefisso");
                    String telefono = (String)application.getAttribute("telefono");
                    session.setAttribute("username", username);
                    session.setAttribute("nome",nome);
                    session.setAttribute("cognome",cognome);
                    session.setAttribute("prefisso",prefisso);
                    session.setAttribute("telefono",telefono);
                }
            }
            
            %>
        <div id="container">
            <%@include file="header.jspf" %>
            <%@include file="nav.jspf" %>
            
            <div id="logininfo">
                <%
                    if(logged){
                        String nome = (String)application.getAttribute("nome");
                        String cognome = (String)application.getAttribute("cognome");
                %>
                
                <h1>Login effettuato correttamente</h1>
                <p>L'utente <%= nome%> <%=cognome%> ha effettuato la login.</p>
                
                <%
                    } else {
                        %>
                
                <h1>Credenziali errate</h1>
                <p>Errore di login.</p>
                        
                        <%
                    }
                    %>
            </div>
            
            <footer>

            </footer>
        </div>
    </body>
</html>
